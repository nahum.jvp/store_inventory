# config MySql
Xamp
instala xamp en https://www.apachefriends.org/es/download.html
Puerto 3306 o por defecto puerto 3308 
instalar xamp
crear la base de datos store_intentory_db
dentro de la base de datos correr el archivo script.sql 

# Config Python
python versión 3.9.0

Entrar en al carpeta app y usar el siguiente comando
pip install -r requirements.txt
# configurar el enviroment
Para crear el entorno virtual con el que trabajaremos debemos tener python instalado
después vamos a escribir los siguientes comandos

# instalar virtualenv en python
pip install virtualenv
# escribir el siguiente comando para crear el enviroment con el que trabajaremos
virtualenv -p python3 env
# entraremos en nuestro enviroment 
.\env\bin source activate
.\env\Scripts\activate
# instalamos las siguientes paqueterías
pip install flask
# después para correr el programa vamos a escribir el siguiente comando
python .\app\app.py
# para conectarnos a la base de datos debemos usar el siguiente comando

pip install Flask-SQLAlchemy
pip install PyMySQL
pip install pandas
pip install openpyxl

set FLASK_DEBUG=TRUE

# Para ubuntu
iniciar xamp 
sudo /opt/lampp/lampp start
# Para iniciar env
cd env
cd bin
source activate envname

Requerimos que se desarrolle un proyecto que permita controlar el inventario de una tienda.
Para eso es necesario contar con un registro de pedidos de productos.

Los productos tienen por lo menos los siguientes atributos:

1. Título
2. SKU
3. Precio de venta

Se deberá considerar que en cada pedido es posible agregar N cantidad de productos.
Al crear los pedidos se debe descontar la cantidad disponible de los productos en el
inventario.

Desafíos

Nivel 1
    Programa usando algún framework de backend que cumpla con lo descrito anteriormente.
Nivel 2
    Crear endpoint GET /orders para consultar el inventario actual permitiendo filtrar por el
    atributo SKU o título.
Nivel 3
    Crear frontend que integre el endpoint /orders para mostrar el inventario actual.

Entregable

1. Repositorio GitLab con el código del proyecto
2. README.md con lo necesario para ejecutarlo en local