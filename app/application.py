from responses.generic_responses import Responses
from flask import Flask
from db import db
from config import Config
from blueprints.category import category_bp
from blueprints.supplier import supplier_bp
from blueprints.product import product_bp
from blueprints.order import order_bp
from blueprints.order_product import order_product_bp
from errors.error_handler import InvalidParamsException, General, InvalidCamp, Id_not_found, key_error
#import pymysql


app = Flask(__name__)
app.config.from_object(Config())

app.register_blueprint(supplier_bp)##
app.register_blueprint(category_bp)##
app.register_blueprint(product_bp)##
app.register_blueprint(order_bp)##
app.register_blueprint(order_product_bp)##
db.init_app(app)

@app.errorhandler(InvalidParamsException)
def error_params(error):
    return Responses.invalid_params(None)

#generico
@app.errorhandler(General)
def error_params_DataError(error):
    return Responses.prueba_DataError(None)

@app.errorhandler(InvalidCamp)
def error_params_DataError(error):
    return Responses.ERROR_CAMP(None)

@app.errorhandler(Id_not_found)
def error_params_DataError(error):
    return Responses.prueba_DataError(None)

@app.errorhandler(key_error)
def error_params_DataError(error):
    return Responses.ERROR_CAMP(None)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
