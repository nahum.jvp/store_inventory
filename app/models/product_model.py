"""Product model"""
from datetime import datetime
from schemas.product_schema import ProductSchema
from db import db
from errors.error_handler import InvalidParamsException

class Product(db.Model):

    id = db.Column(db.Integer, primary_key= True)
    title = db.Column(db.String(30), nullable=False)
    sku = db.Column(db.String(30), nullable=False)
    price = db.Column(db.Float , nullable=False)
    created_at = db.Column(db.DateTime, nullable=True)
    updated_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    OrderProduct = db.relationship('Order_Product', backref='product', lazy=True)##


    fields = (
            "id",
            "title",
            "sku",
            "price"
        )

    # def find_by_params(self, params):
    #     """Get the first coincidence according to the given params"""
    #     return self.query.filter_by(**params).first()

    def __validate__params(self, params):
        """Validate if sent params are valid"""
        for param in params:
            if param not in self.fields:
                raise InvalidParamsException()


    def get_all(self,params=None):
        """Get all non deleted product"""
        self.__validate__params(params)
        return self.query.filter_by(deleted_at=None, **params).all()
    
    def find_by_params(self, params):
        """Get the first coincidence according to the given params"""
        return self.query.filter_by(**params).first()

    def create(self):
        """Create an product using instance of class"""
        self.created_at = datetime.now().strftime("%Y-%m_%d %H:%M:%S")
        db.session.add(self)
        db.session.commit()
    
    def update(self, id, params):
        """Update an product in  DB"""
        product = self.find_by_params({'id': id,'deleted_at': None})
        if product:
            for key, value in params.items():
                setattr(product, key, value)
            db.session.commit()
            return self.find_by_params({'id': id})
        return None

    def destroy(self, id):
        """Destroy an product in  DB"""
        product = self.find_by_params({"id": id})
        if product:
            db.session.delete(product)
            db.session.commit()
            return True
        return None

    def toggle_status(self, params):
        """Change an product status by the given id"""
        product = self.find_by_params(params)
        if product:
            product.deleted_at = None
            db.session.commit()
            return product
        return None
