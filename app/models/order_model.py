"""Order model"""
from datetime import datetime
from schemas.order_schema import OrderSchema
from db import db
from errors.error_handler import InvalidParamsException

class Order(db.Model):

    id = db.Column(db.Integer, primary_key= True)
    delivery_at = db.Column(db.DateTime, nullable=True)
    send_at = db.Column(db.DateTime, nullable=True)
    created_at = db.Column(db.DateTime, nullable=True)
    updated_at = db.Column(db.DateTime, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    OrderProduct = db.relationship('Order_Product', backref='order', lazy=True)##


    fields = (
            "id",
            "delivery_at",
            "send_at"
        )

    # def find_by_params(self, params):
    #     """Get the first coincidence according to the given params"""
    #     return self.query.filter_by(**params).first()

    def __validate__params(self, params):
        """Validate if sent params are valid"""
        for param in params:
            if param not in self.fields:
                raise InvalidParamsException()


    def get_all(self,params=None):
        """Get all non deleted order"""
        self.__validate__params(params)
        return self.query.filter_by(deleted_at=None, **params).all()
    
    def find_by_params(self, params):
        """Get the first coincidence according to the given params"""
        return self.query.filter_by(**params).first()

    def create(self):
        """Create an order using instance of class"""
        self.created_at = datetime.now().strftime("%Y-%m_%d %H:%M:%S")
        db.session.add(self)
        db.session.commit()
    
    def update(self, id, params):
        """Update an order in  DB"""
        order = self.find_by_params({'id': id,'deleted_at': None})
        if order:
            for key, value in params.items():
                setattr(order, key, value)
            db.session.commit()
            return self.find_by_params({'id': id})
        return None

    def destroy(self, id):
        """Destroy an order in  DB"""
        order = self.find_by_params({"id": id})
        if order:
            db.session.delete(order)
            db.session.commit()
            return True
        return None

    def toggle_status(self, params):
        """Change an order status by the given id"""
        order = self.find_by_params(params)
        if order:
            order.deleted_at = None
            db.session.commit()
            return order
        return None
    
    def toggle_send(self, params):
        self.send_at = datetime.now().strftime("%Y-%m_%d %H:%M:%S")
        db.session.add(self)
        db.session.commit()
