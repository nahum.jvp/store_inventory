"""Module for generic responses"""
import json

class Responses:

    @staticmethod
    def success_index(data):
        response = {
            'http_status_code': 200,
            'status': 'SUCCESS INDEX',
            'text' : 'Success index',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 200

    @staticmethod
    def created_success(data):
        response = {
            'http_status_code': 201,
            'status': 'Created',
            'text' : 'Created',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 201

    @staticmethod
    def success_show(data):
        response = {
            'http_status_code': 200,
            'status': 'SUCCESS SHOW',
            'text' : 'SUCCESS SHOW',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 200
    
    @staticmethod
    def not_found(data):
        """Bad response for show request"""
        response = {
            'http_status_code': 404,
            'status': 'Not found',
            'text' : 'Not found',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 404

    @staticmethod
    def success_update(data):
        """Response for success update """
        response = {
            'http_status_code': 201,
            'status': 'Update',
            'text' : 'Update fine',
            'data': data
        }
        return json.dumps(response, ensure_ascii=False), 201

    @staticmethod
    def invalid_params(data):
        """Dab response for show request"""
        response = {
            'http_status_code': 500,
            'status': 'INVALID PARAMS',
            'text': 'the params sent are invalid',
            'data': 'the params sent are invalid'
        }
        return json.dumps(response, ensure_ascii=False), 500

    @staticmethod
    def prueba_DataError(data):
        """Dab response daberror"""
        response = {
            'http_status_code': 500,
            'status': 'Data Error',
            'text': 'the params sent are invalid',
            'data': 'the params sent are invalid'
        }
        return json.dumps(response, ensure_ascii=False), 500
    
    @staticmethod
    def logical_delete(data):
        """Response for logical delete of a resource"""
        response = {
            "statusCode": 200,
            "status": "Deleted",
            "text": "Resource has been successfully deleted",
            "data": data
        }
        return json.dumps(response, ensure_ascii=False)

    @staticmethod
    def send_order(data):
        """Response for send order correct"""
        response = {
            "statusCode": 200,
            "status": "SEND",
            "text": "Send successfully order",
            "data": None
        }
        return json.dumps(response, ensure_ascii=False)


    @staticmethod
    def destroy_resource():
        """Response for a resource destroy"""
        response = {
            "statusCode": 204,
            "status": "Destroyed",
            "text": "Resource has been permanently deleted",
            "data": None
        }
        return json.dumps(response, ensure_ascii=False)

    # @staticmethod
    # def destroy_resource():
    #     """Response for a resource destroy"""
    #     response = {
    #         "statusCode": 204,
    #         "status": "Destroyed",
    #         "text": "Resource has been permanently deleted",
    #         "data": None
    #     }
    #     return json.dumps(response, ensure_ascii=False)
    @staticmethod
    def ERROR_CAMP(data):
        """Dab response error camp"""
        response = {
            'http_status_code': 412,
            'status': ' Precondition failed',
            'text': 'Certain prerequisites specified in the petition are not met fulfilling.',
            'data': 'Wrong data'
        }
        return json.dumps(response, ensure_ascii=False), 412

    def ERROR_VALUE(data):
        """Dab response error camp"""
        response = {
            'http_status_code': 412,
            'status': ' Precondition failed',
            'text': 'Certain prerequisites specified in the petition are not met fulfilling.',
            'data': 'Empy VALUE'
        }
        return json.dumps(response, ensure_ascii=False), 412
    
    def ERROR_exceeded_values(data):
        """Dab response error number of invalid characters"""
        response = {
            'http_status_code': 412,
            'status': ' Precondition failed',
            'text': 'Certain prerequisites specified in the petition are not met fulfilling.',
            'data': 'number of invalid characters'
        }
        return json.dumps(response, ensure_ascii=False), 412

    def NEGATIVE_ID(data):
        """Dab response error camp"""
        response = {
            'http_status_code': 404,
            'status': 'Not found',
            'text': 'Id negative',
            'data': 'NEGATIVE_ID'
        }
        return json.dumps(response, ensure_ascii=False), 412
