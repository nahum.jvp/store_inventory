"""order schema"""
from dataclasses import fields
from ma import ma

class OrderSchema(ma.Schema):
    """orderSchema class"""
    class Meta:
        """Meta Class for orderSchema"""
        fields = (
            "id",
            "delivery_at",
            "send_at"
        )
