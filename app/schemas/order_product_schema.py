"""Schema for order_product"""
from dataclasses import fields
from ma import ma

class order_productSchema(ma.Schema):
    """order_productSchema class"""
    class Meta:
        """Meta Class for order_productSChema"""
        fields = (
            "order_id",
            "product_id",
            "amount",
            "price"
        )
