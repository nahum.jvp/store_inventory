"""product schema"""
from dataclasses import fields
from ma import ma

class ProductSchema(ma.Schema):
    """productSchema class"""
    class Meta:
        """Meta Class for productSchema"""
        fields = (
            "id",
            "title",
            "sku",
            "price"
        )